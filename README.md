# DEB DOSYASI İLE KURULUM
Canon imageFORMULA DR-C130 Tarayıcısının PARDUS'ta çalışması için hazırladığım DEB dosyasıdır. Orjinal dosyanın postinst ve postrm dosyalarında düzenleme yapılmıştır.
* Aşağıdaki komutları uçbirimden sırası ile çalıştırın.
* `wget -nc O- https://kod.pardus.org.tr/yahyayildirim/canon_dr_c130/raw/beta/cndrvsane-drc130_fixed.deb`
* `sudo dpkg --add-architecture i386`
* `sudo apt update`
* `sudo apt install -f ./cndrvsane-drc130_fixed.deb`

Güle güle kullanın...
